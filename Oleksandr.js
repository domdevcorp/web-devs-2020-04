const render = (template, selector) => {
    let target = document.querySelector(selector)
    target.innerHTML = template
}

const joinTemplates = (array, callback) => {
    let template = ''
    for (let i = 0; i < array.length; i++) {
        template += callback(array[i])
    }

    return template
}

function  openMenu() {
    document.getElementById("oleksandr-sidebar").classList.toggle('active');
};

let oleksandr = {name: "Sasha", age: 20, hobbies: ["play field hockey", "play gaming"]}

console.log(oleksandr);

let age = 20;

if (age > 50) {
    console.log('BIG BOY')
} else if (age < 5) {
    console.log('BABY')
} else {
    console.log('perfect')
}

let bmi = 25.3

console.log('My BMI ' + bmi)

let sasha = {weight: 90, height: 1.88};

let BMI = sasha.weight / (sasha.height * sasha.height)

switch (true) {
    case bmi >= 30:
        console.log(`Obese`);
        break;
    case bmi >= 25:
        console.log(`Overveight`);
        break;
    case bmi >= 18.5:
        console.log(`Normal`);
        break;
    default:
        console.log(`Underweight`);
}

let number = 5;

if (number % 2 === 0) {
    console.log('even')
} else {
    console.log('old')
}

const printTriangle = function(symbol) {
    let row = '';

    for (let i = 0; i < 5; i++) {
        row = row + symbol;
        console.log(row + '\n')
    }
};

printTriangle('*');

printTriangle('♦');

printTriangle('•');

(function() {
    console.log('anonimus')
})();

const getFullName = function(a, b) {
    return a + ' ' + b
};

let fullName = getFullName('Oleksandr', 'Zinevych');

console.log('fullName', fullName);

let stick = [
    {
        image: 'https://hockey-webshop.com/wp-content/uploads/2019/06/DY7948_1.jpeg',
        title: "ADIDAS DF24",
        price: 311
    },
    {
        image: 'https://hockey-webshop.com/wp-content/uploads/2019/05/Grays-KN7-Probow-Micro-2019-2020-2019-05-24T014323Z.jpeg',
        title: "GRAYS KN7",
        price: 207
    },
    {
        image: 'https://hockey-webshop.com/wp-content/uploads/2019/06/TK-Total-One-Plus-Gold-2019-06-30T015950Z.jpeg',
        title: "TK TOTAL ONE PLUS",
        price: 269
    },
    {
        image: 'https://hockey-webshop.com/wp-content/uploads/2019/06/Osaka-Pro-Tour-Limited-Proto-Bow-Pre-Order-Delivery-July-2019-06-15T015916Z.jpeg',
        title: "OSAKA PRO TOUR",
        price: 197
    },
];

const getStickTemplate = (stick) => {
    return (
        `<div class="hockey-stick">
            <img class="img-hockey-stick" src="${stick.image}" alt="stick"">
            <h1>${stick.title}</h1>
            <p>price: $ ${stick.price}</p>
        </div>`
    )
}
render(
    joinTemplates(stick, getStickTemplate),
    '#hockey-stick-list'
);

class Devise {
    constructor(name, price) {
        this.name = name
        this.price = price
    }

    walk() {
        console.log(`${this.name}`)
        console.log(`${this.price}`)
    }
}
let Apple = new Devise('iPhone XS MAX', '$999')

let Huawei = new Devise('Huawei p40', '$399')

Apple.walk()
Huawei.walk();

class Now {
    constructor() {
        this.dateTime = new Date()
    }
    getTime() {
        return this.dateTime.toLocaleTimeString()
    }
    getDate () {
        return this.dateTime.toLocaleDateString()
    }
    getTis(){
        return this
    }
}

let now = new Now()

console.log(now.getTis())
console.log(now.getDate())
console.log(now.getTime());

// const Person = function(name) {
//     this.name = name
//     let password = 0
//
//     this.setPassword = function(newPassword) {
//         if (typeof password === 'number' ) {
//             password = newPassword
//         } else {
//             console.error('This is not number')
//         }
//     }
//
//     this.getPassword = function() {
//         return password
//     }
// }
//
// const Sasha = new Person('Sasha')
//
//  Sasha.setPassword(1488)
//
// console.log(Sasha.getPassword())
 let Obj = {
     name: 'Edik',
     age: 45
 }
 console.log(Obj.name)
console.log(Obj['age']);

// let xhr = new XMLHttpRequest()
//
// xhr.open('GET', 'https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits')
//
// xhr.send()
//
// xhr.onload = () => {
//     console.log(JSON.parse(xhr.response))
//     console.log(JSON.parse(xhr.response)[4]['author']['type'])
// };

// fetch('https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits')
// .then(response => response.json())
// .then(console.log);

// const getPersonTemplate = (comment) => {
//     return (
//         `<div class="person">
//             <img src="${comment['author']['avatar_url']}"/>
//             <h1>${comment['author']['login']}</h1>
//             <p class="oleksandr-committer-info">id: ${comment['author']['id']}</p>
//         </div>`
//     )
// };
//
// fetch ('https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits')
//     .then(response => response.json())
//     .then(result =>{
//         console.log(result);
//
//         render(
//             joinTemplates(result, getPersonTemplate),
//             '#people-list'
//         )
//     });
document.cookie = "user=John";

sessionStorage.setItem('hi', '2000');
console.log(sessionStorage.getItem('hi'));
