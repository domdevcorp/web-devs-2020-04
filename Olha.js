console.log('------------------');
console.log('HomeWork 12/06/20');

const render = (template, selector) => {
    let target = document.querySelector(selector);
    target.innerHTML = template
};

const joinTemplates = (array, callback) => {
    let template = '';
    for (let i = 0; i < array.length; i++) {
        template += callback(array[i])
    }
    return template
};

const getPersonTemplate = (comment) => {
    return (
        `<div class="person">
            <img class = "olha-first-image" src="${comment['author']['avatar_url']}"/>
            <h1>${comment['author']['login']}</h1>
            <h3>type: ${comment['author']['type']}</h3>
            <p class="olha-text-card">text: ${comment['commit']['message']}</p>
        </div>`
    )
};

fetch ('https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits')
    .then(response => response.json())
    .then(result =>{
        console.log(result);

    render(
        joinTemplates(result, getPersonTemplate),
        '#people-list'
    )
});




/*console.log('------------------');
console.log('ClassWork 12/06/20');
fetch ('https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits')
    .then(response => response.json())
    .then(result =>{
        console.log(result)
    }) */
/*
console.log('------------------');
console.log('HomeWork 10/06/20');

const render = (template, selector) => {
    let target = document.querySelector(selector);
    target.innerHTML = template
};

const joinTemplates = (array, callback) => {
    let template = '';
    for (let i = 0; i < array.length; i++) {
        template += callback(array[i])
    }
    return template
};

let xhr = new XMLHttpRequest()
xhr.open('GET', 'https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits')
xhr.send()
xhr.onload = () => {
    let comments = JSON.parse(xhr.response)
    console.log(comments)

    render(
        joinTemplates(comments, getPersonTemplate),
        '#people-list'
    )
};

const getPersonTemplate = (comment) => {
    return (
        `<div class="person">
            <img class = "olha-first-image" src="${comment['author']['avatar_url']}"/>
            <h1>${comment['author']['login']}</h1>
            <h3>type: ${comment['author']['type']}</h3>
            <p class="olha-text-card">text: ${comment['commit']['message']}</p>
        </div>`
    )
};
*/

/*console.log('------------------');
console.log('ClassWork 10/06/20');*/

/*let abc = {
    name: 'Taras',
    age: 23,
    'str': 'strVal',
    "str2": "strVal"
}
console.log(abc.name);
console.log(abc['age']);
console.log(abc.str);
console.log(abc.str2);*/

/*
let xhr = new XMLHttpRequest()

xhr.open('GET', 'https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits')

xhr.send()

xhr.onload = () => {
/!*    console.log(xhr)
    console.log(JSON.parse(xhr.response))*!/
    console.log(JSON.parse(xhr.response)[2]['author']['type'])

    let co = JSON.parse(xhr.response)
    console.log(co[3]['sha'])
}

setTimeout ( () => {
    console.log('Hello!')
},7000)*/

/*console.log('------------------');
console.log('ClassWork 07/06/20');*/
/*console.log('------------------');
const Person = function(name){
    this.name = name
    let bankId = 0

    this.setPassword = function(password) {
        if (typeof bankId === 'number') {
            bankId = password
        } else{
            console.error('This is not a number')
        }
    }
    this.getPassword = function() {
        return bankId
    }
}
const Bob = new Person('Bob')
Bob.setPassword(123)
console.log(Bob.getPassword())
console.log(bankId)*/

/*console.log('------------------');
let Person = {
    name: 'Pablo',
    getName(){
        return this.name
    }
};
let Worker = {
    company: 'Google',
    work(){
       return this.company
    }
};
let assign = Object.assign(Person,Worker);
console.log(assign);*/

/*console.log('------------------');
console.log('ClassWork 05/06/20');*/
/*class gadget {
console.log('ClassWork 05/06/20');
class gadget {
    constructor(type, name, color) {
        this.type = type;
        this.name = name;
        this.color = color
    }

    message() {
        console.log('Gadget ', this.name)
    }
}
let iphone = new gadget('iphone', 'Iphone 11', 'black');
let tablet = new gadget('tablet', 'IPad', 'black');

iphone.message()
tablet.message()
*/
/*
class Now {
    constructor() {
        this.now = new Date()
    }
    getTime(){
        return this.now.toLocaleTimeString()
    }
    getDay(){
        return this.now.toLocaleDateString()
    }
}

let now = new Now();

console.log(now.getDay(), '---'now.getTime())

*/

/*
console.log('------------------');
console.log('ClassWork 03/06/20');
const render = (template, selector) => {
    let target = document.querySelector(selector);
    target.innerHTML = template
};

const joinTemplates = (array, callback) => {
    let template = '';
    for (let i = 0; i < array.length; i++) {
        template += callback(array[i])
    }

    return template
};


let people = [
    {
        name: 'Artemon',
        lastName: 'Gig',
        age: 27,
        salary: 1100,
    },
    {
        name: 'Anton',
        lastName: 'Aig',
        age: 17,
        salary: 1000,
    },
    {
        name: 'Bob',
        lastName: 'Big',
        age: 16,
        salary: 900,
    },
    {
        name: 'Artem',
        lastName: 'Arig',
        age: 27,
        salary: 1100,
    },
    {
        name: 'Last',
        lastName: 'But not least',
        age: 27,
        salary: 1100,
    }
]

const animals = [
    {
        kind: 'dog',
        name: 'Chakky',
        breed: 'husky'
    },
    {
        kind: 'cat',
        name: 'Mey',
        breed: 'sphinx'
    }
]

const getAnimalTemplate = (animal) => {
    return (
        ` <div class="animal">
            <h1>${animal.name}</h1>
            <p>Kind: ${animal.kind}</p>
            <p>Breed: ${animal.breed}</p>
        </div>`
    )
};

const getPersonTemplate = (person) => {
    return (
        `<div class="person">
            <h1>${person.name}</h1>
            <h3>Age: ${person.age}</h3>
            <p>Salary: ${person.salary}</p>
        </div>`
    )
};

render(
    joinTemplates(animals, getAnimalTemplate),
    '#animal-list'
)

render(
    joinTemplates(people, getPersonTemplate),
    '#people-list'
)*/

/*
console.log('------------------');
console.log('HomeWork 31/05/20');

const render = (template, selector) => {
    let target = document.querySelector(selector);
    target.innerHTML = template
};

const joinTemplates = (array, callback) => {
    let template = '';
    for (let i = 0; i < array.length; i++) {
        template += callback(array[i])
    }

    return template
};

let products;
products = [
    {
        firstImage: 'images/olha/apple_iphone_se_64gb_white_images_17801797735.png',
        secondImage: 'images/olha/apple_iphone_se_64gb_white_images_17831432779.jpg',
        name: 'Мобильный телефон Apple',
        description: 'iPhone SE 64GB (2020) White',
        oldPrice: '16999 ₴',
        price: '14999 ₴'

    },
    {
        firstImage: 'images/olha/apple_iphone_se_64gb_white_images_17801797735.png',
        secondImage: 'images/olha/apple_iphone_se_64gb_white_images_17831432779.jpg',
        name: 'Мобильный телефон Apple',
        description: 'iPhone SE 64GB (2020) White',
        oldPrice: '16999 ₴',
        price: '14999 ₴'
    },
    {
        firstImage: 'images/olha/apple_iphone_se_64gb_white_images_17801797735.png',
        secondImage: 'images/olha/apple_iphone_se_64gb_white_images_17831432779.jpg',
        name: 'Мобильный телефон Apple',
        description: 'iPhone SE 64GB (2020) White',
        oldPrice: '16999 ₴',
        price: '14999 ₴'
    },
    {
        firstImage: 'images/olha/apple_iphone_se_64gb_white_images_17801797735.png',
        secondImage: 'images/olha/apple_iphone_se_64gb_white_images_17831432779.jpg',
        name: 'Мобильный телефон Apple',
        description: 'iPhone SE 64GB (2020) White',
        oldPrice: '16999 ₴',
        price: '14999 ₴'
    }
];
let productJoined = '';

const getProductTemplate = (product) => {
    return (
        `<div class="olha-product-list">
            <div class = "olha-image-container">
                 <img class = "olha-first-image" src="${product.firstImage}"/>
                 <img class = "olha-second-image" src="${product.secondImage}"/>
             </div>
             <p class="olha-product-title">${product.name}</p> <br>
             <p class="olha-product-title">${product.description}</p>
             <p class="olha-product-old-price ">${product.oldPrice}</p>
             <p class="olha-product-price">${product.price}</p>
        </div>`
    )
};
render(
    joinTemplates(products, getProductTemplate),
    '#products-list'
)
*/
/*
console.log('------------------');
console.log('ClassWork 31/05/20');*/
/*const Taras = {
    name: 'Taras',
    age: 25,
    salary: '5000$'
};
let peopleJoined = '';
const peopleList = document.getElementById('people-list');
peopleList.innerHTML = `<div>
<h1>${Taras.name}</h1>
<h3>age: ${Taras.age}</h3>
<p>salary: ${Taras.salary}</p>
</div>`;*/


/*console.log('------------------');
console.log('HomeTask 29/05/20');
console.log('Task 1');
console.log('___Version 1');
const sumOfNumbers = (arrayOfNumbers) => {
    if(!Array.isArray(arrayOfNumbers)) return;
    let s = 0;
    for (let i = 0; i < arrayOfNumbers.length; i++ ) {
       s += arrayOfNumbers[i];
    }
    return s
};
console.log('sum of numbers in array = ', sumOfNumbers([1,2,3,4]));
console.log('___Version 2 | .reduce');
const sumNumbers = (arrayNumbers) => {
    if(!Array.isArray(arrayNumbers)) return;
    return arrayNumbers.reduce((a, b) => a + b)
};
console.log('sum of numbers in array = ', sumNumbers([1,2,3,4,5,7]));

console.log('ClassWork 29/05/20');
let someId = document.getElementById( "olha-qualities");
console.log('this is id', someId);
let someClass = document.getElementsByClassName("olha-list");
console.log('this are class', someClass[1]);

let count = someClass.length;
for (let i = 0; i < count; i++) {
    console.log(someClass[i].innerHTML);
}
console.log('------------------');
let modal = document.getElementById('myModal');
window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
};*/
/*console.log('------------------');
console.log(' HomeTask 27/05/20');
const displayModal = () => {
    modal.style.display = "flex";
};
const closeModal = () => {
    modal.style.display = "none";
};*/

/*console.log('------------------');
console.log(' ClassWork 27/05/20');
const goIn = () => {
    console.log ('Go in');
};
const goOut = () => {
    console.log ('Go out');
};*/
/*console.log('------------------');
console.log('HomeWork 22/05/20');
const getQuadraticEquationSolution = function(a, b, c) {
    if (! isNaN(a) && ! isNaN(b) && ! isNaN(c)) {
        if (a === 0) {
            return -c / b;
        } else if (c === 0 && b ===0) {
            return 0;
        } else if (b === 0) {
            let m = -c / a;
            if (m < 0) {
                return NaN; /!*'невизначено. Рівняння коренів немає'*!/
            } else {
                return [Math.sqrt(-c / a), -Math.sqrt(-c / a)];
            }
        } else if (c === 0) {
            return [0, -b / a];
        } else {
            let d = (b * b) - (4 * a * c);
            if (d < 0 ) {
                return NaN; /!*"невизначено. D<0. Коренів немає";*!/
            } else if (d === 0) {
                return [ -((-b) / (2 * a) )] /!*'x1 = x2 = ',*!/
            } else
                return [((- b) + Math.sqrt(d)) / (2 * a),((- b) - Math.sqrt(d))/ (2 * a)];
        }
    }
    console.log ('a, b, c - should be numbers')
};
let result = getQuadraticEquationSolution(2, 3, -5)
const readResultsGetQuadraticEquationSolution = function() {
    if (!Array.isArray(result) || !isNaN(result) ) {
        console.log ('Рівняння коренів немає')
    } else
        console.log('x =', result);
};
readResultsGetQuadraticEquationSolution()*/
/*    console.log('------------------');
    console.log('ClassWork 24/05/20');

    const joinArray = (array, separator) => {
       return array.join(separator)
    };
    let fullName = joinArray(['Olha', 'Oleksandrivna', 'Demchuk'], '|');
    let Count = joinArray([1, 2, 3, 4, 5], '|');

    console.log ('Full name is', fullName);
    console.log (Count);*/


/*console.log('------------------');
    console.log('ClassWork ');
const showAlert = () => {
        alert('Hello ');
    }*/
/*    console.log ("Hello, world! :)")
    console.log ("300 + 100 =",300+100)
    console.log ("400 - 200 =",400-200)
    console.log ("400 / 2 =",400/2)
    console.log ("4 * 100 =",4*100)
    console.log ("0/0 =",0/0)
    console.log (6===6)
    console.log ({name:'Olha', mood:'good', learnnew:true})
    console.log ({name:'Olha', hobbies:['photo','traveling','cooking'], learnnew:true})*/

/*    /*console.log('------------------');
    console.log('Hmework ');
let olha = {name:'Olha', isCuteGirl:true, nativeCity:'Vinnytsia'};
    console.log(olha);
    console.log ("*********            ***            *            *    ");
    console.log ("*       *         *      *         ***         *  *   ");
    console.log ("*       *        *        *       *****       *    *  ");
    console.log ("*       *        *        *         *        *      * ");
    console.log ("*       *        *        *         *       *        *");
    console.log ("*       *        *        *         *        *      * ");
    console.log ("*       *        *        *         *         *    *  ");
    console.log ("*       *         *      *          *          *  *   ");
    console.log ("*********           ***             *           *     ");
   /* let count = {first=5,second=7};*/

/*   console.log('------------------');
  let count = 2.7;
  if (count < 1 && count > 0) {
      console.log ('junior')
  } else if (count < 2.5) {
      console.log ('middle')
  } else {
      console.log ('senior')
  }
  console.log('------------------');
  console.log('HmeWork 15/05/20');
  let params = {weight:60, height:1.77} ;
  w = params.weight;
  h = params.height;
  bmi = w/(h*h);
  switch (true) {
      case bmi >= 30:
          console.log('BMI - '+ bmi.toFixed(2) +'. This is obese');
          break;
      case bmi >= 25:
          console.log('BMI - '+ bmi.toFixed(2) +'. This is overweight');
          break;
      case bmi >= 18.5:
          console.log('BMI - '+ bmi.toFixed(2) +'. This is normal');
          break;
      default:
          console.log('BMI - '+ bmi.toFixed(2) +'. This is underweight')
  }*/
/*   if (bmi < 18.5 ) {
       console.log('BMI - '+ bmi.toFixed(2) +'. This is underweight')
   } else if (bmi > 18.5 && bmi < 24.9) {
       console.log('BMI - '+ bmi.toFixed(2) +'. This is normal')
   } else if (bmi > 25 && bmi < 29.9) {
       console.log('BMI - '+ bmi.toFixed(2) +'. This is overweight')
   } else {
       console.log('BMI - '+ bmi.toFixed(2) +'. This is obese')
   }*/
/*   console.log('------------------');
   let now = new Date();
   console.log(now);
   let today = now.getDay();
   switch (today) {
       case 0:
       console.log("Sunday");
           break;
       case 1:
           console.log("Monday");
           break;
       case 2:
           console.log("Tuesday");
           break;
       case 3:
           console.log("Wednesday");
           break;
       default:
           console.log("Some other day")
   }
   console.log('------------------');
   let number = 5;
   count = number % 2;
   switch (count) {
       case 0:
           console.log("The number is even");
           break;
       case 1:
           console.log("The number is odd");
           break;
       default:
           console.log("default")

   } /*
/*    console.log('------------------');
   for (let i = 0; i < 10; i++) {
   let row = ''
   for (let j = 0; j < 10; j++) {
       row += '*'
   }
   console.log(row)
   console.log('\n')
   }*/
/*    console.log('------------------');

    console.log(' Hometask 17/05/20');
    console.log('(a)');
    let t = '';
     for (let i = 0; i < 9; i++ ) {
         for (let j = 0; j <=i; j++) {
            t += '*'
        }
         t += '\n';
    }
    console.log(t);
    console.log('(b)');
    let lines = 9;
    let tr  = '';
    for (let l = 0; l < lines; l++) {
        for (let j = lines-l; j > 0; j-- ) {
            tr += '*'
        }
        tr += '\n';
    }
    console.log(tr);
    console.log('(c)');
    lines = 9;
    t  = '';
    for ( let i = 0; i < lines; i++) {
        for (let j = 0; j < lines; j++ ) {
            if (i > j) {
                t += ' '
            }
            else
                t += '*'
        }
        t += '\n';
    }
    console.log(t);
    console.log('(d)');
    lines = 9;
    t  = '';
    for (let i = 0; i < lines; i++) {
        for (let j = lines-1; j >= 0; j-- ) {
            if (i < j) {
                t += ' '
            }
            else
                t += '*'
        }
        t += '\n';
    }
    console.log(t);*/

/*    console.log('------------------');
    let people = [
        {
            name: 'Bob',
            age: 23,
            salary: 900
        },
        {
            name: 'Anton',
            age: 24,
            salary: 1000
        },
        {
            name: 'Artem',
            age: 27,
            salary: 1100
        },
        {
            name: 'Taras',
            age: 30,
            salary: 1500
        },
        {
            name: 'Olha',
            age: 28,
            salary: 2000
        },
        {
            name: 'Olha',
            age: 18,
            salary: 1200
        }
    ]
    for (let i = 0; i < people.length; i++) {
        if (people[i].name === 'Olha' && people[i].salary > 1500){
            console.log(people[i].name,people[i].salary)
            break
        }*/
/*        console.log('------------------');
        for (let m = 0; m < 10; m++) {
            if (m % 2) {
                continue
            }
        console.log (m)
        }*/
/*    console.log('------------------');
    function printName (name) {
        console.log(name)
    }
    printName('Olha');
    printName('Taras');
    console.log('------------------');

    const secondPrintName = function(name) {
        console.log(name)
    }
    secondPrintName('Olha');
    secondPrintName('Taras');
    console.log('------------------');*/
/*    (function () {
        console.log("Hello");
        var h = "Hi";
        console.log( 'var', h)
    })()*/

