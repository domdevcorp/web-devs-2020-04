let superObject = {
    name: 'superObj',
        child: {
        childName: 'child',
            sayHello() {
            console.log('I am child')
        }
    }
}
// console.log(
//     superObject
// )

superObject.child.sayHello()

let target = window.document.getElementById('target-id')

console.log(target.innerHTML)

let innerText = target.innerHTML
console.log(typeof innerText)

let idFinder = window.document.getElementById('btn')

console.log(idFinder)

let classFinder = window.document.getElementsByClassName('artem-shmartem')

console.log(classFinder[0])

for (let i = 0; i < classFinder.length; i++) {
    console.log(classFinder[i].innerHTML)
}