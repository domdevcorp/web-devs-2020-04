let modal = document.getElementById("myModal");

let btn = document.getElementById("button");

let span = document.getElementsByClassName("kate-close-modal")[0];

btn.onclick = function() {
    modal.style.display = "block";
};

    span.onclick = function() {
    modal.style.display = "none";
};

window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
};

// let targetString = window.document.getElementsByClassName('kate-string');
//
// for (let i = 0; i < targetString.length; i++) {
//     console.log(targetString[i].innerHTML)
// }

const myArray = (numbArray) => {
    let sum = 0;
    for (let i = 0; i < numbArray.length; i++) {
        let addArray = numbArray[i];
        sum += addArray;
    }
    return sum;
};

console.log('Sum =', myArray([0, 2, 2, -3, 2]));

console.log('*********        * * *            *              *');
console.log('*       *      *       *        * * *          *   *');
console.log('*       *     *         *     * * * * *       *     *');
console.log('*       *     *         *         *          *       *');
console.log('*       *     *         *         *         *         *');
console.log('*       *     *         *         *          *       *');
console.log('*       *     *         *         *           *     *');
console.log('*       *      *       *          *            *   *');
console.log('*********        * * *            *              *');

// let height = 1.75;
// let weight = 74;
// let BMI = weight / (height * height);

let kate = {name: 'Kate', height: 1.75, weight: 74};

let BMI = kate.weight / (kate.height * kate.height);

console.log('BMI', BMI);

if (BMI >= 30) {
    console.log('This is obese')
} else if (BMI >= 25) {
    console.log('This is overweight')
} else if (BMI >= 18.5) {
    console.log('This is normal')
} else {
    console.log('This is underweight')
}

console.log('===');

switch (true) {
    case BMI >= 30:
        console.log('This is obese');
        break;
    case BMI >= 25:
        console.log('This is overweight');
        break;
    case BMI >= 18.5:
        console.log('This is normal');
        break;
    default:
        console.log('This is underweight');
}

let number = 4;

if (number % 2 === 0) {
    console.log('This number is even')
} else {
    console.log('This number is odd')
}

let numbers = [1, 2, 3];

console.log(numbers[2]);

// let people = [
//     {
//         name : 'Bob',
//         age: 23,
//         salary: 900
//     },
//     {
//         name: 'Stepan',
//         age: 18,
//         salary: 200
//     },
//     {
//         name: 'Ivan',
//         age: 27,
//         salary: 800
//     },
//     {
//         name: 'Kate',
//         age: 25,
//         salary: 3000
//     },
//     {
//         name: 'Corgi',
//         age: 3,
//         salary: 30000
//     },
// ];
//
// for (let i = 0; i < people.length; i++) {
//     if (people[i].name === 'Kate') {
//         console.log('Kate says Hello', people[i], i)
//     }
// }

// const person = {
//     name: 'Kate',
//     age: 26,
//     salary: 1000,
// };
//
// const peopleList = document.getElementById('people-list');
//
// peopleList.innerHTML = `<div class="person"></div><h1>${person.name}</h1><h3>${person.age}</h3><p>${person.salary}</p></div>`;

let dishes = [
    {
        name: 'Тарілка Orner "Коргі"',
        img: 'https://i2.rozetka.ua/goods/8281649/orner-0473_images_8281649596.jpg',
        price: '199₴',
    },
    {
        name: 'Тарілка Orner "Шпіц"',
        img: 'https://i2.rozetka.ua/goods/15137938/orner_0881_images_15137938873.jpg',
        price: '209₴',
    },
    {
        name: 'Тарілка Orner "Єдиноріг"',
        img: 'https://i2.rozetka.ua/goods/15138097/orner_0863_images_15138097699.jpg',
        price: '239₴',
    },
    {
        name: 'Тарілка Orner "Кіт з піцою"',
        img: 'https://i1.rozetka.ua/goods/2341602/orner_0080_images_2341602889.jpg',
        price: '259₴',
    },
];

const getDishesTemplate = (tableware) => {
    return (
        `<div class="kate-tableware">
            <h1>${tableware.name}</h1>
            <img class="kate-tableware-img" src="${tableware.img}" alt="dish"">
            <h2>Ціна: ${tableware.price}</h2>
        </div>`
    )
};

const render = (template, id) => {
    let target = document.getElementById(id);

    target.innerHTML = template
};

let dishesJoined = '';

for (let i = 0; i < dishes.length; i++) {
    dishesJoined += getDishesTemplate(dishes[i])
}

render(dishesJoined, 'tableware-list');


const printTriangle = function(symbol) {
    let row = '';

    for (let i = 0; i < 10; i++) {
        row = row + symbol;
        console.log(row + '\n')
    }
};

printTriangle('😍');

printTriangle('♥');

(function() {
    console.log('I Am Anonymous')
})();

console.log('Quadratic equation');

const getQuadraticEquationSolution = (a, b, c) => {
    if (a === 0) return NaN;
    if (typeof a !== 'number' || typeof b !== 'number' || typeof c !== 'number') return NaN;
    let D = b * b - 4 * a * c;
    let x1, x2;
    if (D > 0) {
        x1 = (-b + Math.sqrt(D)) / (2 * a);
        x2 = (-b - Math.sqrt(D)) / (2 * a);
        return [''+ x1 +', '+ x2 + '']
    } else if (D === 0) {
        return (-b / (2 * a));
    } else if (D < 0)
        return NaN;
};
console.log('x =', getQuadraticEquationSolution(1, 5, 3));

// const getFullName = function(a, b) {
//     return a + ' ' + b
// };
// console.log('My full name is', getFullName('Kateryna', 'Volchasta'));
//
// const getFullName = function(fullName, separator) {
//     return fullName().join(separator)
//
// };
// let fullName = ['Volchasta', 'Kateryna', 'Volodymyrivna'];
//
// console.log(fullName, separator('*'));
// console.log(fullName, separator(':'));
//
const joinArrayBySeparator = (nameslist, separator) => {
    return nameslist.join(separator)
};

let fullName = ['Volchasta', 'Kateryna', 'Volodymyrivna'];
let numb = [1, 2, 3];

console.log(joinArrayBySeparator(fullName, ('*')));
console.log(joinArrayBySeparator(fullName, (' ')));
console.log(joinArrayBySeparator(fullName, ('♥')));

console.log(joinArrayBySeparator(numb, ('*')));
console.log(joinArrayBySeparator(numb, (' ')));
console.log(joinArrayBySeparator(numb, ('♥')));

let a = '';

for (let i = 0; i < 10; i++) {
    a += '*';
    console.log(a + '\n')
}

console.log('===');

for (let i = 0; i < 10; i++) {
    let b = '';
    for (let j = 10; j > i; j--) {
        b += '*'
    }
    console.log(b + '\n')
}

console.log('===');

for (let i = 0; i < 10; i++) {
    let c = '';
    for (let j = 0; j < i; j++) {
        c += ' ';
    }
    for (let j = 10; j > i; j--) {
        c += '*';
    }
    console.log(c + '\n')
}

console.log('===');

for (let i = 0; i < 10; i++) {
    let d = '';
    for (let j = 9; j > i; j--) {
        d += ' ';
    }
    for (let j = 0; j <= i; j++) {
        d += '*';
    }
    console.log(d += '\n')
}

console.log('===');

function openNav() {
    document.getElementsByClassName("kate-sidenav")[0].style.display = "block";
    document.getElementsByClassName("kate-main-content")[0].style.marginLeft = '17%';
    document.getElementById("close-button").style.display = "block";
    document.getElementById("open-button").style.display = "none";
}

function closeNav() {
    document.getElementsByClassName("kate-sidenav")[0].style.display = "none";
    document.getElementsByClassName("kate-main-content")[0].style.marginLeft = "0";
    document.getElementById("close-button").style.display = "none";
    document.getElementById("open-button").style.display = "block";
}

// let people = [
//     {
//         name: 'Artemon',
//         lastName: 'Gig',
//         age: 27,
//         salary: 1100,
//     },
//     {
//         name: 'Anton',
//         lastName: 'Aig',
//         age: 17,
//         salary: 1000,
//     },
//     {
//         name: 'Bob',
//         lastName: 'Big',
//         age: 16,
//         salary: 900,
//     },
//     {
//         name: 'Artem',
//         lastName: 'Arig',
//         age: 27,
//         salary: 1100,
//     },
//     {
//         name: 'Last',
//         lastName: 'But not least',
//         age: 27,
//         salary: 1100,
//     }
// ]

//
// const animals = [
//     {
//         kind: 'dog',
//         name: 'Chakky',
//         breed: 'Corgi'
//     },
//     {
//         kind: 'cat',
//         name: 'Mey',
//         breed:'Maine Coon'
//     }
// ]
//
// const getAnimalTemplate = (animal) => {
//     return (
//         ` <div class="animal">
//             <h1>${animal.name}</h1>
//             <p>Kind: ${animal.kind}</p>
//             <p>Breed: ${animal.breed}</p>
//         </div>`
//     )
// };
//
// const getPersonTemplate = (person) => {
//     return (
//         `<div class="person">
//             <h1>${person.name}</h1>
//             <h3>Age: ${person.age}</h3>
//             <p>Salary: ${person.salary}</p>
//         </div>`
//     )
// };
//
// const render = (template, selector) => {
//     let target = document.querySelector(selector)
//     target.innerHTML = template
// }
//
// const joinTemplates = (array, callback) => {
//     let template = ''
//     for (let i = 0; i < array.length; i++) {
//         template += callback(array[i])
//     }
//
//     return template
// }
//
// render(
//     joinTemplates(animals, getAnimalTemplate),
//     '#animal-list'
// )
//
// render(
//     joinTemplates(people, getPersonTemplate),
//     '#people-list'
// )

// class Dog {
//     constructor (name, breed) {
//         this.dogsname = name;
//         this.dogsbreed = breed;
//     }
//     display() {
//         console.log('Hello! I am ' + this.dogsbreed + ' and my name is '  + this.dogsname)
//         }
// }
//
// let Corgi = new Dog('Korzhyk', 'corgi');
//
// let Labrador = new Dog('Labradorzhyk', 'labrador');
//
// Corgi.display();
// Labrador.display();

// let moment = {
//     getTime() {
//         return new Date().toLocaleTimeString()
//     },
//     getDate() {
//         return new Date().toLocaleDateString()
//     }
// };
// console.log(moment.getDate(), '\n', moment.getTime());

let Dog = {
    name: 'Korzhyk',
    getName() {
        return this.name
    }
};

let Corgi = {
    walk() {
        console.log('I can walk')
    }
};

console.log(Object.assign(Dog, Corgi));

const SocialNetwork = function(name) {
    this.name = name;
    let password = 0;

    this.setPassword = function(newPassword) {
        if (typeof password === 'number') {
            password = newPassword
        } else {
            console.error ('Not a number')
        }
    };

    this.getPassword = function() {
        return password
    }
};

const Instagram = new SocialNetwork('Instagram');

Instagram.setPassword(2020);

console.log(Instagram.getPassword());
console.log(Instagram.password);

let prince = {
    age: 99,
    name: 'Philip'
};
console.log(prince.age);
console.log(prince['age']);

// console.log('hahaha');
//
// console.log('hi');
//
// setTimeout(() => {
//     console.log(`Hello asynchron`)
// }, 5000);

// let xhr = new XMLHttpRequest();
//
// xhr.open('GET', 'https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits');
//
// xhr.send();
//
// xhr.onload = () => {
//     // console.log(xhr);
//     let comments = JSON.parse(xhr.response);
//
//     console.log(comments);
// console.log(comments[2]['author']['type'])

const dateToLocal = (date) => {
    return new Date(date).toLocaleString()
};

const getCommentsTemplate = (committer) => {
    return (
        `<div class="kate-comments">
                <h1>${committer['author']['login']}</h1>
                <img class="kate-committer-image" src="${committer['author']['avatar_url']}" alt="photo">
                <p class="kate-committer-info">Name: ${committer['commit']['author']['name']}</p>
                <p class="kate-committer-info">id: ${committer['author']['id']}</p>
                <p class="kate-committer-info">Date: ${dateToLocal(committer['commit']['author']['date'])}</p>
            </div>`
    );
};

const renderData = (template, list) => {
    let target = document.getElementById(list);

    target.innerHTML = template
};

const joinTemplates = (array, callback) => {
    let template = '';
    for (let i = 0; i < array.length; i++) {
        template += callback(array[i])
    }

    return template
};

fetch('https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits')
    .then(response => response.json())
    .then((comments) => {
        console.log(comments);

        renderData(
            joinTemplates(comments, getCommentsTemplate),
            'list'
        );
    });

sessionStorage.setItem('cool_developers', 'hello');

console.log(sessionStorage.getItem('cool_developers'));