let modal = document.getElementById("lazyModal");
let btn = document.getElementById("lazyButton");
let span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
  modal.style.display = "block";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
let target = window.document.getElementsByClassName("lazy-class");
console.log(target);
let idTarget = window.document.getElementById("lazyClose");
console.log(idTarget);

for(let i = 0; i < target.length; i++) {
    console.log(target[i].innerHTML);
}

/* Write a function that takes an array of numbers and returns the sum of them.*/
let getRandomArray = [...Array(10)].map(() => Math.floor(Math.random() * 100));
document.body.innerText = getRandomArray;
for(let i = 1; i <= 100; i++){
  getRandomArraySum = getRandomArray.reduce(function(a, b) { 
    return a + b; 
  }, 0);
}
console.log(getRandomArray,'Sum is', getRandomArraySum);