function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}

const render = (template, selector) => {
    let target = document.querySelector(selector)
    target.innerHTML = template
}
const joinTemplates = (array, callback) => {
    let template = ''
    for (let i = 0; i < array.length; i++) {
        template += callback(array[i])
    }

    return template
}
let people = [
    {
        name: 'Artemon',
        lastName: 'Gig',
        age: 27,
        salary: 1100,
    },
    {
        name: 'Anton',
        lastName: 'Aig',
        age: 17,
        salary: 1000,
    },
    {
        name: 'Bob',
        lastName: 'Big',
        age: 16,
        salary: 900,
    },
    {
        name: 'Artem',
        lastName: 'Arig',
        age: 27,
        salary: 1100,
    },
    {
        name: 'Last',
        lastName: 'But not least',
        age: 27,
        salary: 1100,
    }
]

const animals = [
    {
        kind: 'dog',
        name: 'Chakky'
    },
    {
        kind: 'cat',
        name: 'Mey'
    }
]

const getAnimalTemplate = (animal) => {
    return (
        ` <div class="animal">
            <h1>${animal.name}</h1>
            <p>Kind: ${animal.kind}</p>
        </div>`
    )
};

const getPersonTemplate = (person) => {
    return (
        `<div class="person">
            <h1>${person.name}</h1>
            <h3>Age: ${person.age}</h3>
            <p>Salary: ${person.salary}</p>
        </div>`
    )
};
render(
    joinTemplates(animals, getAnimalTemplate),
    '#animal-list'
)

render(joinTemplates(people, getPersonTemplate),
    '#people-list'
)
class Person {
    constructor(name, age) {
        this.name = name
        this.age = age
    }
    move() {
        console.log(`I am ${this.name}and I cannot swim`)

    }
}
let Helenaaaa = new Person('Helenaaaa', 28)
let Bysya = new Person('Bysya', 37)

Helenaaaa.move()
Bysya.move()


class Now {
    constructor() {
        this.dateTime = new Date()
    }
    getTime(){
        return this.dateTime.toLocaleTimeString()
    }
    getDate(){
        return this.now.toLocaleDateString()
    }
}
let Human = {
    name: 'Garry',
    getName: function () {
        return this.name

    }
}
console.log(Human)

let Sportsmen = {
    name: 'Sportsmen',
    work: function () {
        console.log('I like to run and swim')

    },
}
let Lawyer = {
    name: 'Lawyer',
}
console.log(Object.assign(Human,Sportsmen,Lawyer))
 const Saleman = function (name) {
    this.name = name
     let password = 0
     this.setPassword = function (newPassword) {
         if (typeof password === 'number') {
             password = newPassword
     } else  {
         console.error('This is not a number')
     }
    }
    this.getPassword = function () {
        return this.password

    }
 }


let Objk = {
    blabla:'test',
    'stringKey': 'stringValue',
    "stringKey2": "stringValue2"
}
console.log(Objk['blabla'])
console.log(Objk.stringKey)
console.log(Objk["stringKey2"])

let xhr = new XMLHttpRequest()
xhr.open('GET', 'https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits')
xhr.send()
xhr.onload = () => {

    let comments = JSON.parse(xhr.response)
    console.log(xhr)

console.log(comments[2]['author']['type'])

}
 console.log('some log')

setTime( () => {
    console.log( 'Hi')
}, 0)
console.log('after timeout')