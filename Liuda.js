let books = [
    {
        name: 'Дж. К. Роулинг "Случайная вакансия"',
        price: '80₴',
        imageSrc: 'images/Liuda/1.png',
    },
    {
        name: 'Ліна Костенко Триста поезій',
        price: '130₴',
        imageSrc: 'images/Liuda/2.png',
    },
    {
        name: 'Джордж Оруэлл Антиутопия "1984"',
        price: '115₴',
        imageSrc: 'images/Liuda/3.png',
    },
    {
        name: 'Кузьма Скрябін Проза. Поезія',
        price: '145₴',
        imageSrc: 'images/Liuda/4.png',
    },
    {
        name: 'Ювал Ной Харарі "Homo Deus"',
        price: '182₴',
        imageSrc: 'images/Liuda/5.png',
    }
]
const getBookTemplate = (book) => {
    return (
        `<div class="book">
            <h3>${book.name}</h3>
            <h3 class="book-price">Price: ${book.price}</h3>
             <img class="book-image" src="${book.imageSrc}" alt="book"">
        </div>`
    )
}
const render = (template, id) => {
    let target = document.getElementById(id)
    target.innerHTML = template
}
let booksJoined = ''
for (let i = 0; i < books.length; i++) {
    booksJoined += getBookTemplate(books[i])
}
render(booksJoined, 'books-list')
const animals = [
    {
        kind: 'dog',
        name: 'Chakky',
        breed: 'poodle',
    },
    {
        kind: 'dog',
        name: 'Luna',
        breed: 'husky',
    }
]
const getAnimalTemplate = (animal) => {
    return (
        ` <div class="animal">
            <h1>${animal.name}</h1>
            <p>Kind: ${animal.kind}</p>
            <p>Breed: ${animal.breed}</p>
        </div>`
    )
};

render
    joinTemplates(animals, getAnimalTemplate), ('#animal-list')
// const Liuda = {
//     name: 'Liudmyla',
//     lastName: 'Khodakivska',
//     age: '29',
//     salary: '3000S'
// }
// const peopleList = document.getElementById('people-list');
// peopleList.innerHTML = `<h1>${Liuda.name}</h1><h3>${Liuda.age}</h3><p>${Liuda.salary}</p>`
// function openNav() {
//     document.getElementById("sidebarLiuda").style.width = "250px";
//     document.getElementById("main").style.marginLeft = "250px";
// }
// function closeNav() {
//     document.getElementById("sidebarLiuda").style.width = "0";
//     document.getElementById("main").style.marginLeft = "0";
// }
// // const array = [1, 2, 3, 4, 5];
// // const sum = array.reduce(function(a, b) {
// //     return a + b;
// // }, 0);
// // console.log(sum);
// const array = (arrayOfNumbers) => {
//     let sum = 0;
//     for (let i = 0; i < arrayOfNumbers.length; i++) {
//          let sumArray = arrayOfNumbers[i];
//         sum += sumArray;
//     }
//     return sum;
// };
// console.log(array([-135, 7, 56, 11, 20, 1111, -13, 2, 6, 24, 55, 1, 4, 18, -9, 0, -23]));
// const showAlert = () => {
//     alert('Hello from function')
// }
// const modal = document.querySelector(".modal");
// const trigger = document.querySelector(".trigger");
// const closeButton = document.querySelector(".close-button");
// function toggleModal() {
//     modal.classList.toggle("show-modal");
// }
// function windowOnClick(event) {
//     if (event.target === modal) {
//         toggleModal();
//     }
// }
// trigger.addEventListener("click", toggleModal);
// closeButton.addEventListener("click", toggleModal);
// window.addEventListener("click", windowOnClick);
// let age = 29;
// if (age > 65) {
//     console.log('You are pensioner');
// } else if (age < 18) {
//     console.log('You are underaged');
// } else {
//     console.log('You are superwoman');
// }
// let liuda = {height: 1.73, weight: 59};
// let BMI = 59 / (1.73 * 1.73)
// switch (true) {
//     case BMI >= 30:
//         console.log('Obese', BMI);
//         break;
//     case BMI >= 25:
//         console.log('Overweight', BMI);
//         break;
//     case BMI >= 18.5:
//         console.log('Normal', BMI);
//         break;
//     default:
//         console.log('Underweight', BMI)
// }
// let number = 8;
// let result = number % 2;
// if (result === 0) {
//     console.log('Number is even');
// } else {
//     console.log('Number is odd');
// }
// let people = [
//     {
//         name : 'Bob',
//         age: 23,
//         salary: 9000
//     },
//     {
//         name: 'Sam',
//         age: 17,
//         salary: 6000
//     },
//     {
//         name: 'Tom',
//         age: 22,
//         salary: 7000
//     },
//     {
//         name: 'Liuda',
//         age: 29,
//         salary: 20000
//     },
//     {
//         name: 'Vicky',
//         age: 15,
//         salary: 3000
//     },
// ];
// for (let i = 0; i < people.length; i++) {
//     if (people[i].age < 18) {
//         console.log('Underaged', people[i].age, i)
//     }
// }
// let rowA = '';
// for (let i = 0; i < 10; i++) {
//     rowA += '*';
//     console.log(rowA + '\n')
// }
// for (let i = 0; i < 10; i++) {
//     let rowB = '';
//     for (let j = 10; j > i; j--) {
//         rowB += '*';
//     }
//     console.log(rowB + '\n')
// }
// for (let i = 0; i < 10; i++) {
//     let rowC = '';
//     for (let j = 0; j < i; j++) {
//         rowC += '';
//     }
//     for (let j = 10; j > i; j--) {
//         rowC += '*';
//     }
//     console.log(rowC + '\n')
// }
// for (let i = 0; i < 10; i++) {
//     let rowD = '';
//     for (let j = 9; j > i; j--) {
//         rowD += '';
//     }
//     for (let j = 0; j <= i; j++) {
//         rowD += '*';
//     }
//     console.log(rowD += '\n')
// }
// let stars = [
//     '*********         ***           *          *     ',
//     '*       *       *     *        ***        * *    ',
//     '*       *      *       *      *****      *   *   ',
//     '*       *      *       *        *       *     *  ',
//     '*       *      *       *        *      *       * ',
//     '*       *      *       *        *       *     *  ',
//     '*       *      *       *        *        *   *   ',
//     '*       *       *     *         *         * *    ',
//     '*********         ***           *          *     ',
// ]
// let figures = stars.join('\n')
// console.log(figures);
// function printName(name) {
//     console.log(name)
// }
// printName('Liuda')
// printName('Liudmyla')
// printName('Bob')
// printName('Tom')
// const printTriangle = function(symbol) {
//     let row = ' '
//     for (let i = 0; i < 10; i++) {
//         row = row + symbol
//         console.log(row + '\n')
//     }
// }
// printTriangle('*')
// printTriangle('#')
// printTriangle('$')
// (function () {
//     var a = 'Hello'
//     console.log(a)
// })();
// const getQuadraticEquationSolution = function(a, b, c) {
//     if (a === 0) {
//         console.log('a cannot equal zero');
//     } else if (a = NaN) {
//         console.log('a must be a number');
//     }
// } else if (b = NaN) {
//     console.log('b must be a number');
// } else if (c = NaN) {
//     console.log('c must be a number');
// } else {
//     console.log('Calculate the result using x = (-b +- Math.sqrt(b * b - 4 * a * c)) / 2 * a')
//     let D = b * b - 4 * a * c
//     if (D > 0) {
//         console.log('x1 = ' (-b + Math.sqrt(D)) / 2 * a; "x2 = " (-b - Math.sqrt(D)) / 2 * a);
//     } else if  (D = 0) {
//         console.log('x = ' (-b + Math.sqrt(D)) / 2 * a || "x = " (-b - Math.sqrt(D)) / 2 * a);
//     } else if (D < 0) {
//     }
//     console.log('No root');
// }
// const makeFullName = (name, surname) => {
//     return name + ' ' + surname
// }
// let fullName = makeFullName('Liudmyla', 'Khodakivska')
// console.log('My full name is', fullName);
// </script>
// <script>
// let fullName = ['Liudmyla', 'Khodakivska', 'Anatoliyivna']
// let fullNameCat = ['Khodakivska', 'Mashka', 'Anatoliyivna']
// const makeFullName = (listName, separator) => {
//     return listName.join(separator)
// }
// console.log(makeFullName(fullName,'#'));
// console.log(makeFullName(fullNameCat,'|'));
// let target = window.document.getElementById('target')
// console.log('target-id', target)
// let traits = window.document.getElementsByClassName('liuda-traits-list')
// console.log(traits)
// let classes = window.document.getElementsByClassName('liuda-traits-list')
// for (let i = 0; i < classes.length; i++) {
// console.log(classes[i].innerHTML)
// }