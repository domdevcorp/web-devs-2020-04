console.log('==================hometask 14-05-2020=====================');
console.log('        *********        * * *            *              *');
console.log('        *       *      *       *        * * *          *   *');
console.log('        *       *     *         *     * * * * *       *     *');
console.log('        *       *     *         *         *          *       *');
console.log('        *       *     *         *         *         *         *');
console.log('        *       *     *         *         *          *       *');
console.log('        *       *     *         *         *           *     *');
console.log('        *       *      *       *          *            *   *');
console.log('        *********        * * *            *              *');
console.log(' ');

console.log('==================hometask 15-05-2020=====================');
let oleg = {name:'oleg', weight: 75, height: 1.71};
console.log (oleg);
let BMI = [oleg.weight/(oleg.height*oleg.height)];
console.log('Your Body mass index =', BMI);
if (BMI < 18.5) {
    console.log('You are underweight')
} else if (BMI >= 18.5 && BMI <= 24.9) {
    console.log('You are normal weight')
} else if (BMI >= 24.9 && BMI <= 29.9) {
    console.log('You are overweight')
} else {
    console.log('You are obese')
}
console.log(' ');

console.log('==================calsswork=====================');
let number = 61;
if (number % 2 === 0) {
    console.log('the number is even')
} else {
    console.log('the number is odd')
}
console.log(' ');

console.log('==================hometask 17-05-2020=====================');
for (let i = 0; i < 10; i++) {
    let row = '';
    for (let j = 0; j <= i; j++) {
        row += '*'
    }
    console.log(row + '\n')
}
console.log(' ');
for (let i = 9; i >= 0; i--) {
    let row = '';
    for (let j = 0; j <= i; j++) {
        row += '*'
    }
    console.log(row + '\n')
}
console.log(' ');
for (let i = 9; i >= 0; i--) {
    let space = '';
    let row = '';
    for (let k = 10; k >= i; k--) {
        space += ' '
    }
    for (let j = 0; j <= i; j++) {
        row += '*';
    }
    console.log(space + row + '\n')
}
console.log(' ');
for (let i = 0; i < 10; i++) {
    let space = '';
    let row = '';
    for (let k = 10; k >= i; k--) {
        space += ' '
    }
    for (let j = 0; j <= i; j++) {
        row += '*';
    }
    console.log(space + row + '\n')
}
console.log(' ');

console.log('==================calsswork=====================');
//урок приклад як знайти боба в списку об'єктів і перервати цикл пошуку після успішного результату
let people = [
    {
        name: 'Bob',
        age: 25,
        salary: 900
    },
    {
        name: 'Anton',
        age: 21,
        salary: 1000
    },
    {
        name: 'Artem',
        age: 24,
        salary: 1100
    },
    {
        name: 'Margaret',
        age: 30,
        salary: 1100
    },
    {
        name: 'David',
        age: 19,
        salary: 2000
    },
    {
        name: 'Oleg',
        age: 26,
        salary: 4000
    }
];
//можна вивести окремі списки з масива
for (let i = 0; i < 3;  i++) {
    console.log(people[i].age)
}
//або використати умову. .length - це свойство для масива яке = кількості його елементів. для того щоб зупинитись після досягнутого результату і не шукати даліб використовується break
for (let i = 0; i < people.length; i++) {
    if (people[i].salary >= 4000) {
        console.log('People with salary more or equal than 4000', people[i], i);
        break
    }
}
//щоб знайти тільки парні числа використовуєм % (ділення з остачою)
for (let i = 1; i < 10; i++) {
    if (!(i % 2)) {
        console.log('i', i)
    }
}
//також є інший спосіб знайти парні числа за допомогою умов та ключового слова continue
for (let i = 1; i < 10; i++) {
    if (i % 2) {
        continue
    }
    console.log(i)
}
// об'явлення змінних
{
    let superGlobal = 'superGlobal';
    console.log('super',superGlobal);
}
let loop = 0;
for (loop; loop < 5; loop++) {
    console.log('loop', loop)
}
console.log('loop',loop);
let square = function(number) {
    for (let i = 0; i < 5; i++) {
        let row = '';
        for (let j = 0; j < 5; j++) {
            row += number
        }
        console.log(row);
        console.log('\n')
    }
};
square(1);
(function () {
    var func = 'deprecated function';
    console.log(func)
})();
console.log(' ');

console.log('==================hometask 22-05-2020=====================');
const getQuadraticEquation = function(a,b,c) {
    let D = Math.pow(b, 2) - 4 * a * c;
    if (a === 0)
        return x = ['for a quadratic equation (', a, 'x ^ 2 +', b, 'x +', c, '= 0) there are no solutions because coefficient a can not equal 0'].join(' ');
    else if (b === 0 || c===0)
        return x = ['for a quadratic equation (', a, 'x ^ 2 +', b, 'x +', c, '= 0) there are no solutions because coefficient b or c = 0, so the equation is pure quadratic'].join(' ');
    else if (D > 0) {
        return  x = ['for a quadratic equation (', a, 'x ^ 2 +', b, 'x +', c, '= 0) there are two solutions:', ' x1 =', (- b + Math.sqrt(D)) / (2 * a), 'x2 =', (- b - Math.sqrt(D)) / (2 * a)].join(' ');
    } else if (D === 0) {
        return  x = ['for a quadratic equation (', a, 'x ^ 2 +', b, 'x +', c, '= 0) there is one solution:', ' x =', - b / (2 * a)].join(' ');
    } else if (D < 0) {
        return  x = ['for a quadratic equation (', a, 'x ^ 2 +', b, 'x +', c, '= 0) there are no solutions because the discriminant is less than 0'].join(' ');
    }
    else
        return x = ['you put the wrong symbols'].join(' ')
};
console.log(getQuadraticEquation(5, 5, -3));
console.log(' ');

console.log('==================calsswork=====================');
const fullname = function(a,b) {
    return a + ' ' + b
};
let myfullname = fullname('Oleg', 'Bonchuk');
console.log('my full name is ', myfullname);

const getfullname = function(separator) {
    let fullname = ('Bonchuk-Oleg-Mykolaiovich');
    return fullname.split('-').join(separator)
};
console.log(getfullname('___'));
console.log( );

console.log('==================hometask 27-05-2020=====================');
let modal = document.getElementsByClassName("modal")[0];
console.log(modal)

console.log('==================hometask 29-05-2020=====================');
const array = [123, 132, 213, 231, 312, 321];
sum = array.reduce(function(a, b){
    return a + b;
});
console.log(sum);

function togglesidebar() {
    document.getElementsByClassName("oleg-sidebar")[0].classList.toggle("active");
}

console.log('==================hometask 31-05-2020=====================');
