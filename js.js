
let listOfNumber = [1,2,5,8,7,9,10,20,5,8]
let total = 0
const countOfNumbers =() => {
   for (let i in listOfNumber) {
      total += listOfNumber[i]
   }
   console.log(total)
}
countOfNumbers()

let counter = 0
const buttonFun = () => {
   counter++
   let element = document.getElementById("navtoggle");
   element.classList.add("active");
   let elements = document.getElementById("nav");
   elements.classList.add("active");
   console.log(counter)

   if (counter % 2 === 0) {
      let element = document.getElementById("nav");
      element.classList.remove("active");
      let elements = document.getElementById("navtoggle");
      elements.classList.remove("active");
   }
}

const render = (template, selector) => {
   let target = document.querySelector(selector);
   target.innerHTML = template
};

const joinTemplates = (array, callback) => {
   let template = '';
   for (let i = 0; i < array.length; i++) {
      template += callback(array[i])
   }
   return template
};

let product;
product = [
   {
      image: 'images/Nikita/xiaomi.jfif',
      name: "Мобільний телефон Xiaomi",
      describe: 'Xiaomi Note  8 32GB 2019 Green',
      oldPrice: '20000 ₴',
      newPrice: '18999 ₴'
   },
   {
      image: 'images/Nikita/xiaomi.jfif',
      name: 'Мобільний телефон Xiaomi',
      describe: 'Xiaomi Note  8 32GB 2019 Green',
      oldPrice: '20000 ₴',
      newPrice: '18999 ₴'
   },
   {
      image: 'images/Nikita/xiaomi.jfif',
      name: 'Мобільний телефон Xiaomi',
      describe: 'Xiaomi Note  8 32GB 2019 Green',
      oldPrice: '20000 ₴',
      newPrice: '18999 ₴'
   }
];

const getProductTemplate = (product) => {
   return (
       `<div class="product-list">
            <div class = "image">
                 <img class = "first-image" src="${product.image}"/>
             </div>
             <p class="product-title">${product.name}</p> <br>
             <p class="product-title">${product.describe}</p>
             <p class="product-old-price">${product.oldPrice}</p>
             <p class="product-price">${product.newPrice}</p>
        </div>`
   )
};
render (
    joinTemplates(product, getProductTemplate),
    '#products-list'
)

/*let xhr = new XMLHttpRequest()
xhr.open('GET', 'https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits')
xhr.send()
xhr.onload = () => {
   let dog =JSON.parse((xhr.response))
   console.log(dog)
}
*/

fetch ('https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits')
    .then(response => response.json())
    .then(result =>{
       console.log(result);

       render(
           joinTemplates(result, getPersonTemplate),
           '#templateArray'
       )
    });

const getPersonTemplate = (info) => {
   return (
       `<div class="templateXHR">
            <p class="text"> ${info['commit']['message']}</p>
        </div>`
   )
};
document.cookie = "frank=dog";

sessionStorage.setItem('cool_fight','java')
console.log(sessionStorage.getItem('cool_fight'))